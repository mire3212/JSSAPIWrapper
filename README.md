# JSSWrapperAPI
### Summary
Let me start by saying, I'm not a professional coder, just a hobbyist who's been interested in coding for Apple products for a long time! A lot of my code is trial and error, Google and Stack Overflow.

This class is meant to act as a wrapper to easily interface with the JSS API to allow seamless integration with the CasperSuite and your Swift application.

### Functionality
Currently only a few actions have been created:

- `init(serverURL: String, jamfUsername: String, jamfPassword: String)` accepts the username and password to initiate the connection to the JSS
- `getComputerRecord(serialNumber: String, completionHandler: (NSData?, NSURLResponse?, NSError?) -> Void)` accepts both the serial number of the system in String format as well as a completion handler that allows you to deal with the response from the server however you see fit.
- `createPlaceholder(serialNumber: String, macAddress: String, completionHandler: (NSData?, NSURLResponse?, NSError?) -> Void)` accepts the serial and MAC address of a computer (usually found on the outside of the box or sometimes available from a shipper) to create a "Place Holder" Mac in the JSS. This allows you to pre-populate static groups with devices that you know will enroll into the JSS to allow specific policies to take place at enrollment. Think DEP without DEP.
- `addComputerToGroup(serialNumber: String, groupName: String, completionHandler: (NSData?, NSURLResponse?, NSError?) -> Void)` allows you to add a computer (including the placeholder) into an existing Static Group. Accepts the serial and a completion handler
- `removeComputerFromGroup(serialNumber: String, groupName: String, completionHandler: (NSData?, NSURLResponse?, NSError?) -> Void)` allows you to remove a computer. Accepts the Serial and completion handler.
- `getGroups(completionHandler: (NSData?, NSURLResponse?, NSError?) -> Void)` retrieves a list of all groups.

### Future Expectations
Currently the code requires you to handle the returned data to allow you to do what you want with the XML that's returned from the JSS. In future implemetnations, I'd like to overload these methods to allow you to either chose to provide the completion handler, or to accept value in native data formats.

For example, I would like to have an `NSDictionary` (or Swift `Dictionary`) returned for the `getComputerRecord` function or a `List` for the `getGroups` function. This would probably involve using a (preferably built-in) XML parser.

In addition to adding these overloaded methods, more wrappers are needed for other JSS API functions.

### Notes

You can find information regarding the JSS API calls by visitng your local JSS API. This URL is `https://yourjssurl.com/api`, or if you're cloud hosted, `https://jss.jamfcloud.com/yourjss/api`.